# Task 3. GIT basics

We have 3 main operations in this task
- Join commits
- Split commit in two
- Move a commit in commit history between different branches 

For join, we use interactive rebase with squash option.

For commit split, we need to set edit option during the interactive rebase and reset the commit we want to split in two. After that, we can make as many new commits as we'd like.

To move commit from another brunch I have used cherry-pick command. After the cherry-pick, I have changed the order of the commits with interactive rebase.

This is how the repository graph looks like before the final step - merge f2 to the master branch.

![screenshot](Code_4yjv9KXOJW.png)

<details>
<summary>Reflog</summary>
<ul>
<li>ecabd5b (HEAD -> master, f2) HEAD@{0}: merge f2: Fast-forward</li>
<li>c3b6a2a HEAD@{1}: checkout: moving from f1 to master</li>
<li>487d0a9 (f1) HEAD@{2}: rebase (continue) (finish): returning to refs/heads/f1</li>
<li>487d0a9 (f1) HEAD@{3}: rebase (continue): c10</li>
<li>d3b35ac HEAD@{4}: rebase (pick): c8</li>
<li>75c63eb HEAD@{5}: rebase (start): checkout 75c63eb9c31366c9b1c39dd51f739c1e307ab710</li>
<li>fe9f635 HEAD@{6}: checkout: moving from f2 to f1</li>
<li>ecabd5b (HEAD -> master, f2) HEAD@{7}: rebase (finish): returning to refs/heads/f2</li>
<li>ecabd5b (HEAD -> master, f2) HEAD@{8}: rebase (pick): c9</li>
<li>ec28e3c HEAD@{9}: rebase (pick): с7(2) cherry picked</li>
<li>f10cb41 HEAD@{10}: rebase (start): checkout c3b6a2a8b8f25a58370bb1364b67ffb30a603b7c</li>
<li>9159a91 HEAD@{11}: commit (cherry-pick): с7(2) cherry picked</li>
<li>1a7004e HEAD@{12}: checkout: moving from f1 to f2</li>
<li>fe9f635 HEAD@{13}: rebase (continue) (finish): returning to refs/heads/f1</li>
<li>fe9f635 HEAD@{14}: rebase (continue): c10</li>
<li>fe23135 HEAD@{15}: rebase (continue) (pick): c8</li>
<li>37296c0 HEAD@{16}: commit: с7(2)</li>
<li>75c63eb HEAD@{17}: commit: с7(1)</li>
<li>f616d3c HEAD@{18}: reset: moving to HEAD^</li>
<li>d591ed2 HEAD@{19}: commit (amend): c7(1)</li>
<li>b13814d HEAD@{20}: rebase: fast-forward</li>
<li>f616d3c HEAD@{21}: rebase (start): checkout f616d3c</li>
<li>ecc8e33 HEAD@{22}: checkout: moving from f2 to f1</li>
<li>1a7004e HEAD@{23}: rebase (finish): returning to refs/heads/f2</li>
<li>1a7004e HEAD@{24}: rebase (pick): c9</li>
<li>f10cb41 HEAD@{25}: rebase (squash): c5 c6 squash</li>
<li>8abe1df HEAD@{26}: rebase (start): checkout c3b6a2a</li>
<li>f724810 HEAD@{27}: checkout: moving from f1 to f2</li>
<li>ecc8e33 HEAD@{28}: commit: c10</li>
<li>8fb39ba HEAD@{29}: checkout: moving from f2 to f1</li>
<li>f724810 HEAD@{30}: commit: c9</li>
<li>91a27ab HEAD@{31}: checkout: moving from f1 to f2</li>
<li>8fb39ba HEAD@{32}: commit: c8</li>
<li>b13814d HEAD@{33}: checkout: moving from f2 to f1</li>
<li>91a27ab HEAD@{34}: checkout: moving from f1 to f2</li>
<li>b13814d HEAD@{35}: commit: c7</li>
<li>f616d3c HEAD@{36}: checkout: moving from f2 to f1</li>
<li>91a27ab HEAD@{37}: commit: c6</li>
<li>8abe1df HEAD@{38}: reset: moving to 8abe1df3a9eb8588a31e65ab224ae71130783f48</li>
<li>a180f57 HEAD@{39}: reset: moving to a180f57badbc024a8381095d2417c0299c266e31</li>
<li>a180f57 HEAD@{40}: checkout: moving from f1 to f2</li>
<li>f616d3c HEAD@{41}: checkout: moving from f2 to f1</li>
<li>a180f57 HEAD@{42}: commit: c6</li>
<li>8abe1df HEAD@{43}: checkout: moving from f1 to f2</li>
<li>f616d3c HEAD@{44}: checkout: moving from f2 to f1</li>
<li>8abe1df HEAD@{45}: commit: c5</li>
<li>c3b6a2a HEAD@{46}: checkout: moving from f1 to f2</li>
<li>f616d3c HEAD@{47}: commit: c4</li>
<li>c3b6a2a HEAD@{48}: checkout: moving from master to f1</li>
<li>c3b6a2a HEAD@{49}: commit: c3</li>
<li>e883192 HEAD@{50}: commit: c2</li>
<li>e280c30 HEAD@{51}: commit (initial): c1</li>
</ul>
</details>